#!/usr/bin/env python
# coding: utf-8
import numpy as np
import os
from glob import glob
import warnings
from netCDF4 import Dataset
import subprocess
import time
import datetime

# Find path to specific file (there likely is a better way to do this...)
def findFile(path0,fileStrIdentifier):
    '''Obtain string containing the absolute path to file'''
    glob_pattern = os.path.join(path0, fileStrIdentifier)
    try:
        path = sorted(glob(glob_pattern), key=os.path.getctime)[0]
    except:
        raise ValueError('No files satisfying the condition')
    return path

# Select ROI (Region of Interest)
def selectROI(lat,lon,boxRoi):
    '''Compute the square subset of pixels in the along/across track geometry (L1B rows and cols) that encloses the requested square in lat-lon geometry'''
    [R0,C0] = np.shape(lat)

    if np.shape(lat) != np.shape(lon):
        warnings.warn('Lat and Lon matrices should be the same size!')

    roiCond = (lat<boxRoi['N'])&(lat>boxRoi['S'])&(lon<boxRoi['E'])&(lon>boxRoi['W'])
    roiCondLin = np.where(roiCond)
    
    if boxRoi['N'] < boxRoi['S']:
        raise ValueError('North limit must be northward of the south limit!')
    if boxRoi['E'] < boxRoi['W']:
        raise ValueError('East limit must be eastward of the West limit!')
    if len(roiCondLin[0]) == 0:
        raise ValueError('ROI falls out of current scene!')
    
    [rmin,rmax] = [np.max([np.min(roiCondLin[0])-2,0]),np.min([np.max(roiCondLin[0])+2,R0])]
    [cmin,cmax] = [np.max([np.min(roiCondLin[1])-2,0]),np.min([np.max(roiCondLin[1])+2,C0])]
    if (rmin==0)|(rmax==R0)|(cmin==0)|(cmax==C0):
        warnings.warn('ROI falls partially out of scene!')

    ROI = {'rm':rmin,'rM':(rmax+1),'cm':cmin,'cM':(cmax+1),'R0':R0,'C0':C0,'R':rmax+1-rmin,'C':cmax+1-cmin}
    return ROI

# Slice radiance values in track geometry (row-col) according to computed ROI
def sliceFromROI(matrix,ROI):
    '''
    Slice raster "matrix" according to the region of interest specified in ROI.
    "matrix" should be the same size as the original "lat" and "lon" matrices from which the ROI in row-col geometry was calculated
    '''
    if np.shape(matrix) != (ROI['R0'],ROI['C0']):
        raise ValueError('Input matrix should be %i x %i!' % (ROI['R0'],ROI['C0']))

    matROI = matrix[ROI['rm']:ROI['rM'],ROI['cm']:ROI['cM']]

    return matROI

# Store derived rasters at netCDF
def img2NetCDF4(name,path,varNc,description):
    ''' Store in netCDF4 file 'path/name' the matrices stored at varNc.
    'varNc' is a dictionary containing dictionaries itself with 
    the netcdf variable names, values and description.
    
    e.g.

    varNc = {'lat': {'val':im['lat'],'desc':'Latitude' ,'units':'degree_north [WGS84+DEM]'},\
             'lon': {'val':im['lon'],'desc':'Longitude','units':'degree_east  [WGS84+DEM]'}}
    
    ALL the matrices are assumed to have the same dimensions r x c'''
    print(path)
    print(os.path.join(path,name))
    try:
        #subprocess.call(['chmod','-R', '+w', path]) # blo removed; unreliable on windows machines
        out_file=Dataset(os.path.join(path,name),'w', format="NETCDF4")
    except:
        raise ValueError('NetCDF already created! Delete the existing one and re-run')

    dimFlag = True
    for v in varNc.keys():
        if dimFlag:
            r = out_file.createDimension('r', np.shape(varNc[v]['val'])[0])
            c = out_file.createDimension('c', np.shape(varNc[v]['val'])[1])
            dimFlag = False

        out_file.description = description
        out_file.history = 'Created ' + time.ctime(time.time()) 

        v0 = out_file.createVariable(v, np.float32,('r','c'))
        v0[:] = varNc[v]['val']
        v0.units = varNc[v]['units']
        v0.desc = varNc[v]['desc']
    out_file.close()

# Get time stamp from OLCI image filename
def OLCIDateUTC(path,preString):
    '''Get time stamp in UTC from OLCI image filename.
    PATH is the absolute path to the image.
    PRESTRING is the string that appears immediately before the acquisition time info inside the filename'''
    # Slice file name to get acquisition time:
    i0 = path.find(preString) + len(preString)
    dateImg = path[i0:(i0+15)]
    dateImg = dateImg.replace('T','')

    print('"' + dateImg + '" has format: ' + str(type(dateImg)))

    # Translate it to "timeStamp"
    dateImg = datetime.datetime.strptime(dateImg, "%Y%m%d%H%M%S")

    print('"' + str(dateImg) + '" has now format: ' + str(type(dateImg)))

    return dateImg
